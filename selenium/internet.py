import pytest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class Selectors:
    HOMEPAGE_TITLE = "h1"
    DROPDOWN_PAGE_TITLE = "h3"
    DROPDOWN = "#dropdown"
    LINK_DROPDOWN = "#content > ul > li:nth-child(11) > a"

@pytest.fixture(scope="module")
def setup():
    driver_service = Service(ChromeDriverManager().install())
    driver = webdriver.Chrome(service=driver_service)
    driver.maximize_window()    
    yield driver
    driver.quit()

# @pytest.mark.skip(reason="Skipping this test")
def test_page_title(setup):
    driver = setup
    url = "http://the-internet.herokuapp.com"
    driver.get(url)

    # waiting for the page to load LOL
    try:
        page_title = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, Selectors.HOMEPAGE_TITLE)))
        expected_title = "Welcome to the-internet"
        assert page_title.text == expected_title
    except Exception as e:
        pytest.fail(f"Failed to validate homepage title: {str(e)}")

def test_drop_down(setup):
    driver = setup
    url = "http://the-internet.herokuapp.com"
    driver.get(url)

    driver.find_element(By.CSS_SELECTOR, Selectors.LINK_DROPDOWN).click()

    dropdown_page_title = driver.find_element(By.CSS_SELECTOR, Selectors.DROPDOWN_PAGE_TITLE)
    expected_dropdown_page_title = "Dropdown List"
    assert dropdown_page_title.text == expected_dropdown_page_title

    dropdown = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, Selectors.DROPDOWN)))
    select = Select(dropdown)

    select.select_by_visible_text("Option 2")
    time.sleep(2)

    # Getting all options
    options = select.options

    # Assertions
    assert len(options) == 3, "Expected three options in the dropdown"
    if len(options) == 3:
        print("dropdown assertion 1 PASSED")

    assert options[0].text == "Please select an option", "First option should be 'Please select an option'"

    if options[0].text == "Please select an option":
        print("dropdown assertion 2 PASSED")

    assert options[-1].text == "Option 2", "Last option should be 'Option 2'"
